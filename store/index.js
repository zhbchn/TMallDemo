import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		hasLogin: uni.getStorageSync('hasLogin')?true:false,
		userInfo:uni.getStorageSync('userInfo')
	},
	mutations: {
		login(state, provider) {
			state.hasLogin = true;
			state.userInfo = provider;
			uni.setStorageSync('userInfo',provider);
			uni.setStorageSync('hasLogin',state.hasLogin);
			console.log(state.userInfo);
		},
		logout(state) {
			state.hasLogin = false;
			state.userInfo = '';
			uni.removeStorageSync('userInfo');
			uni.removeStorageSync('hasLogin');
		}
	},
	actions: {
	
	}
})

export default store

# 电商模版

## 说明

> 后台连接了jeecg-boot只实现了登录、记录登录状态，显示登录用户信息和退出登录功能，其他功能没有改变。
> 其他功能是使用uniapp的电商模版
> 整合了极简登录模版，地址：(https://ext.dcloud.net.cn/plugin?id=538)，完成后了登录jeecg-boot的功能。

![](README_files/1.png)
![](README_files/2.png)
![](README_files/3.png)

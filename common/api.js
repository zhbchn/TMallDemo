import store from '../store/index';
import {appConfig} from './config'
import {token} from './token.js'

const send = (url, data = {}, method = 'POST', showLoading = true, base_url = '') => {
    uni.showLoading({
        title: '加载中'
    })
    return new Promise((resolve) => {
        uni.request({
            method: method,
            url: (base_url ? base_url : appConfig.apiUrl) + url,
            data: data,
            header: (() => {
                const tokeValue = token.get()
                let config = {}
                if (tokeValue) {
                    config[appConfig.sendTokenKey] = tokeValue
                }
				config['content-type']='application/json'
                return config
            })(),
            success: (res) => {
                uni.hideLoading()
                resolve(res.data)
            }
        })
    })
}
export default {
	/**
	 * 接口名称
	 * apiName（obj）
	 */
	apiName: {
		login: 'single/home/login', 		// 登录(手机号:phone 密码:password)
	},

	/**
	 * 封装请求（async await 封装uni.request）
	 * method	   post/get		
	 * endpoint    接口方法名
	 * data		   所需传递参数	
	 * load		   是否需要loading
	 */
	async apiCall(method, endpoint, data,load) {
		if (!load) {
			uni.showLoading({
				title: '请稍候',
				mask: true
			});
		}
		let fullurl = appConfig.apiUrl + endpoint;
		let Authorization = token.get();
		let [error, res] = await uni.request({
			url: fullurl,
			data: data,
			method: method,
			header: {
				//'Content-Type': 'application/x-www-form-urlencoded',
				'content-type': 'application/json',
				'X-Access-Token':  Authorization || ''
			},
		});
		
		if (!load) {
			uni.hideLoading();
		}		
		console.log(res);
		// if (res.data.code == 200) {
		// 	return res.data;
		// }
		// else{
		// 	uni.showToast({
		// 		title: res.data.msg,
		// 		icon: 'none'
		// 	});	
		// }
	},
	/**
	 * 封装请求（async await 封装uni.request）
	 * method	   post/get		
	 * endpoint    接口方法名
	 * data		   所需传递参数	
	 * load		   是否需要loading
	 */
	async login( data,load) {
		if (!load) {
			uni.showLoading({
				title: '请稍候',
				mask: true
			});
		}
		let fullurl = appConfig.apiUrl + '/sys/mLogin';
		//let Authorization = `${store.state.userInfo.tokenHead}${store.state.userInfo.token}`;
		let [error, res] = await uni.request({
			url: fullurl,
			data: data,
			method: 'POST',
			header: {
				//'Content-Type': 'application/x-www-form-urlencoded',
				'content-type': 'application/json',
				//'Authorization':  Authorization || ''
			},
		});
		if (!load) {
			uni.hideLoading();
		}		
		if (res.data.code == 200) {
			return res.data;
		}
		else{
			uni.showToast({
				title: res.data.msg,
				icon: 'none'
			});	
		}
	},




}

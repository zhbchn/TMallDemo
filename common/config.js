export const appConfig = {
    tokenKey: 'token',
	sendTokenKey:'X-Access-Token',
    apiUrl: process.env.NODE_ENV === 'development' ? 'http://192.168.0.106:8080/jeecg-boot' : ''
}
